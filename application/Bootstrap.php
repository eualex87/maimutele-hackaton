<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initConstants() {
        $constants = new Zend_Config_Ini(APPLICATION_PATH . '/configs/constants.ini');
        Zend_Registry::set('constants', $constants);
    }

    protected function _initFront() {
        $this->bootstrap('frontController');
    }

    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }


    protected function _initScript()
    {
        $this->view->headScript()
            ->prependFile( "/js/jquery-1.11.0.min.js", $type = 'text/javascript' );
    }

    protected function _initStyle()
    {
        $this->view->headLink()
            ->prependStylesheet( "/css/global.css" )
            ->prependStylesheet('/css/bootstrap.min.css')
            ->prependStylesheet('/css/justified-nav.css');
    }
}

