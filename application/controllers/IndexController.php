<?php

require_once APPLICATION_PATH.'/forms/UploadForm.php';

class IndexController extends Zend_Controller_Action
{
    protected $_authors = array(
        'Andrei Liutec' => '',
        'Claudiu Rusu' => '',
        'Alexandru Turbatu' => 'Cimpanzeul Barbos',
        'Adrian Stefan Bancu' => 'King Kong',
        'Octavian Bonea' => ''
    );

    protected $_acceptedMatchPercentage = 11;

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body

    }

    public function uploadAction(){
        //var_dump(dpf_test_param_string('value for param1', 'value for param2'));
        //var_dump(dpf_test_return_array('value sent as param'));
        //$a =dpf_compare_images('C:\Hackaton2014\apache\htdocs\public\picture_library\30784-total_war_rome_2_wallpaper.jpg' ,  'C:\Hackaton2014\apache\htdocs\public\picture_library\30784-total_war_rome_2_wallpaper.jpg', 'C:\Hackaton2014\apache\htdocs\public\picture_library\30784-total_war_rome_2_wallpaper_test.jpg', 10);
        //var_dump($a);
        //var_dump(APPLICATION_PATH.'\..\public\picture_library\30784-total_war_rome_2_wallpaper.jpg');
        //var_dump(file_exists(APPLICATION_PATH.'/../public/picture_library/30784-total_war_rome_2_wallpaper.jpg'));
        //var_dump(dpf_compare_images(APPLICATION_PATH.'\..\public\picture_library\30784-total_war_rome_2_wallpaper.jpg' ,APPLICATION_PATH.'\..\public\picture_library\30784-total_war_rome_2_wallpaper.jpg' ,APPLICATION_PATH.'\..\public\picture_library\30784-total_war_rome_2_wallpaper.jpg', 10));

        $form = new UploadForm();
        $form->setAction($this->view->url())
             ->setMethod('post');

        $request = $this->getRequest();
        $post = true;
        if(!$request->isPost() || !$form->isValid($request->getPost())){
            $post = false;
        }

        if($post){
            try{
                //upload complete
                $success = $form->file->receive();
                if($success){
                    $this->view->uploadSuccess = true;
                    //make the picture able to be seen
                    $pictureInfo = $form->file->getFileInfo();
                    $picturePath = $pictureInfo['file']['destination'].'/'.$pictureInfo['file']['name'];
                    chmod($picturePath, 0777);
                    $picturePathMatch = $pictureInfo['file']['destination'].'/match.jpg';
                    //var_dump($picturePath);die;
                    //$result = dpf_compare_images( $picturePath ,  $picturePath , $picturePathMatch, 10);
                    $result = 11;
                    $matchedImagesArray = array();
                    if($handle = opendir(PICTURE_LIB_PATH)) {
                        $iteration = intval(file_get_contents(PUBLIC_PATH.'/iteration'));
                        while(false !== ($entry = readdir($handle))){
                            if(is_file(PICTURE_LIB_PATH . "/". $entry) && $entry != $pictureInfo['file']['name']){
                                $libraryImagePath = PICTURE_LIB_PATH . "/". $entry;
                                $matchedImagePoints = PICTURE_MATCHED_PATH . "/test".$iteration.'.jpg';
                                //$result = dpf_compare_images( $picturePath ,  $libraryImagePath , $matchedImagePoints, $this->_acceptedMatchPercentage);
                                $result = 11;

                                if($result >= $this->_acceptedMatchPercentage){
                                    $relativePathWithMatchingPoints= "/picture_matched/test".$iteration.'.jpg';
                                    $relativePathMatched = "/picture_library/".$entry;

                                    $matchedImagesArray[] = array(  'matchedPercentage'=>$result,
                                        'libraryPicture'=>$relativePathMatched,
                                        'matchingPointPicture'=>$relativePathWithMatchingPoints
                                    );
                                    $iteration++;
                                }
                            }
                        }
                        file_put_contents(PUBLIC_PATH.'/iteration',$iteration);
                    }


                    $this->view->picturePath = '/picture_library/'.$pictureInfo['file']['name'];
                    $this->view->matchedImagesArray = $matchedImagesArray;
                }
                else {
                    $this->view->uploadSuccess = false;
                    $this->view->uploadError = $form->file->getErrorMessages();
                }

                $this->view->hasPost = true;



            } catch (Exception $exception) {
                //error uploading file
                $this->view->form = $form;
            }
        }
        else{
            $this->view->hasPost = false;
        }


        $this->view->form = $form;
    }

    public function imageLibraryAction() {
        $this->view->headLink()->appendStylesheet('/css/blueimp-gallery.min.css');
        $this->view->headLink()->appendStylesheet('/css/bootstrap-image-gallery.min.css');
        $images = $this->processImageLibrary('picture_library');
        if (count($images) > 0) {
            $this->view->images = $images;
        }
    }

    public function pictureMatchedLibraryAction() {
        $this->view->headLink()->appendStylesheet('/css/blueimp-gallery.min.css');
        $this->view->headLink()->appendStylesheet('/css/bootstrap-image-gallery.min.css');
        $images = $this->processImageLibrary('picture_matched');
        if (count($images) > 0) {
            $this->view->images = $images;
        }
    }

    private function processImageLibrary($library_name) {
        $scanned_dir = array_diff(scandir(APPLICATION_PATH.'/../public/'.$library_name), array('..', '.', 'thumbnails'));
        $images = array();
        $image_library_path = $library_name . '/';
        $image_library_thumbnail_path = $image_library_path . 'thumbnails/';
        foreach ($scanned_dir as $value) {
            $image = array();
            $image['src'] = $image_library_path . $value;
            $image['src_thumbnails'] = $image_library_thumbnail_path . $value;
            $this->resize_image($image['src'], $image_library_thumbnail_path . substr($image['src'], strrpos($image['src'], '/')), 200, 200);
            $image['title'] = 'image';
            $images[] = $image;
        }

        return $images;
    }

    private function resize_image($file, $destination, $w, $h) {
        //Get the original image dimensions + type
        list($source_width, $source_height, $source_type) = getimagesize($file);

        //Figure out if we need to create a new JPG, PNG or GIF
        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        if ($ext == "jpg" || $ext == "jpeg") {
            $source_gdim=imagecreatefromjpeg($file);
        } elseif ($ext == "png") {
            $source_gdim=imagecreatefrompng($file);
        } elseif ($ext == "gif") {
            $source_gdim=imagecreatefromgif($file);
        } else {
            //Invalid file type? Return.
            return;
        }

        //If a width is supplied, but height is false, then we need to resize by width instead of cropping
        if ($w && !$h) {
            $ratio = $w / $source_width;
            $temp_width = $w;
            $temp_height = $source_height * $ratio;

            $desired_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled(
                $desired_gdim,
                $source_gdim,
                0, 0,
                0, 0,
                $temp_width, $temp_height,
                $source_width, $source_height
            );
        } else {
            $source_aspect_ratio = $source_width / $source_height;
            $desired_aspect_ratio = $w / $h;

            if ($source_aspect_ratio > $desired_aspect_ratio) {
                /*
                 * Triggered when source image is wider
                 */
                $temp_height = $h;
                $temp_width = ( int ) ($h * $source_aspect_ratio);
            } else {
                /*
                 * Triggered otherwise (i.e. source image is similar or taller)
                 */
                $temp_width = $w;
                $temp_height = ( int ) ($w / $source_aspect_ratio);
            }

            /*
             * Resize the image into a temporary GD image
             */

            $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled(
                $temp_gdim,
                $source_gdim,
                0, 0,
                0, 0,
                $temp_width, $temp_height,
                $source_width, $source_height
            );

            /*
             * Copy cropped region from temporary image into the desired GD image
             */

            $x0 = ($temp_width - $w) / 2;
            $y0 = ($temp_height - $h) / 2;
            $desired_gdim = imagecreatetruecolor($w, $h);
            imagecopy(
                $desired_gdim,
                $temp_gdim,
                0, 0,
                $x0, $y0,
                $w, $h
            );
        }

        /*
         * Render the image
         * Alternatively, you can save the image in file-system or database
         */

        if ($ext == "jpg" || $ext == "jpeg") {
            ImageJpeg($desired_gdim,$destination,100);
        } elseif ($ext == "png") {
            ImagePng($desired_gdim,$destination);
        } elseif ($ext == "gif") {
            ImageGif($desired_gdim,$destination);
        } else {
            return;
        }

        ImageDestroy ($desired_gdim);
    }

    public function authorsAction()
    {
        $this->view->authors = $this->_authors;
    }


}

