<?php
class UploadForm extends Twitter_Form
{
    public function init()
    {
        $this->setAttrib('enctype', 'multipart/form-data');
        $this->setMethod('post');

        /*$description = new Zend_Form_Element_Text('description');
        $description->setLabel('Description')
            ->setRequired(true)
            ->addValidator('NotEmpty');
        $this->addElement($description);*/

        $file = new Zend_Form_Element_File('file');
        $file->setLabel('File to upload:')
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->addValidator('Count', false, 1)
            ->addValidator('Extension', false, 'jpg,png,gif')
            ->setDestination(PICTURE_LIB_PATH);
        $this->addElement($file);

        $this->addElement('submit', 'submit', array(
            'label'    => 'Upload',
            'ignore'   => true
        ));
    }
}